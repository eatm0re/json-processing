package edu.eatmore.jsonprocessing;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

public class Customer {

  private String firstName;
  private String lastName;
  private int age;
  private Address address;
  private Phone[] phones;

  public Customer() {
  }

  public Customer(JSONObject json) {
    firstName = json.getString("firstName");
    lastName = json.getString("lastName");
    age = json.getInt("age");
    address = new Address(json.getJSONObject("address"));
    JSONArray jsonArray = json.getJSONArray("phoneNumbers");
    phones = new Phone[jsonArray.length()];
    for (int i = 0; i < phones.length; i++) {
      phones[i] = new Phone(jsonArray.getJSONObject(i));
    }
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Phone[] getPhones() {
    return phones;
  }

  public void setPhones(Phone[] phones) {
    this.phones = phones;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", age=" + age +
        ", address=" + address +
        ", phones=" + Arrays.toString(phones) +
        '}';
  }
}

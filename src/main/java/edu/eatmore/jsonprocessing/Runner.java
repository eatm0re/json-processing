package edu.eatmore.jsonprocessing;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Runner {

  public static void main(String[] args) throws IOException {
    String fileName = "example.json";
    Path path = Paths.get(fileName);
    byte[] bytes = Files.readAllBytes(path);
    String fileContent = new String(bytes);
    JSONObject json = new JSONObject(fileContent);
    Customer customer = new Customer(json);
    System.out.println(customer);
  }
}

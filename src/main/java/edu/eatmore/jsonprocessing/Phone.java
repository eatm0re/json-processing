package edu.eatmore.jsonprocessing;

import org.json.JSONObject;

public class Phone {

  private String type;
  private String number;

  public Phone() {
  }

  public Phone(JSONObject json) {
    type = json.getString("type");
    number = json.getString("number");
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  @Override
  public String toString() {
    return "Phone{" +
        "type='" + type + '\'' +
        ", number='" + number + '\'' +
        '}';
  }
}
